# White-box-pentesting
 This lab is created to demonstrate pass-the-hash, blind sql and SSTI vulnerabilities

## Original repo
https://github.com/TROUBLE-1/White-box-pentesting

## Mod by Vay3t & Ksha
+ Dockerized

# Description for the vulnerability
- 2nd order blind-boolean based sql injection in registration form which results in users_logs if true all logs will be shown else empty
- Got admin username and password in hash which can't be cracked makes it difficult to bypass authentication
- Understang the Authentication system can help to find a misconfiguration which leads to pass-the-hash and admin account takeover
- An SSTI vulnerability in admin at search option which leads to RCE

### [Watch this quick Video](https://youtu.be/FMymBuWHwjE)

## Your goal
- Do a source code review to find the following vulnerabilities and get logged in admin's account.
- And get a reverse shell.
- You can email me the walkthrough of your findings and scripts for exploiting.

email id: raunakparmar786@gmail.com

#### difficult level - Intermediate

## Configuration in config.php
 Replace mysql credentials with your credentials
 ```
 <?php
    define('DB_SERVER', 'localhost');
    define('DB_USERNAME', 'user');
    define('DB_PASSWORD', 'password');
    define('DB_DATABASE', 'trouble1');
    $db = mysqli_connect(DB_SERVER,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
 ?>
 ```
 # Create mysql user
Run the following commands in mysql
```
CREATE USER 'user'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON * . * TO 'user'@'localhost';
```
 
## setup database
```
root@kali:/var/www/html# service mysql start
root@kali:/var/www/html# mysql -u root -p -e "create database trouble1"
root@kali:/var/www/html# mysql -u root -p trouble1 < lab.sql
```
###### contact me on twitter for any help https://twitter.com/trouble1_raunak

### [Walkthrough](https://www.youtube.com/watch?v=wL_P-TndgXg)
