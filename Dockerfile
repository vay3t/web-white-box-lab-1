FROM php:7.4.28-apache

RUN docker-php-ext-configure mysqli && \
    docker-php-ext-install mysqli

WORKDIR /var/www/html/
COPY ./trouble1_whiteBox .
RUN sed -i 's/localhost/database/g' config.php && \
    sed -i '1i <?php session_start(); ?>' index.php
