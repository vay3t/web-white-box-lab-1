
--
-- Table structure for table `admin`
--

create table `admin`(
	`id` int primary key auto_increment, 
	`username` text, 
	`password` text);

--
-- Table structure for table `users`
--

create table `users`(
	`id` int primary key auto_increment, 
	`username` text, 
	`password` text);

--
-- Dumping data for table `admin`
--

insert into `admin`(username, password) values 
('trouble1','21ffccddf815587ff8149a8efb1a5d86ac295d19');

--
-- Table structure for table `user_logs`
--

create table `user_log`(
	`id` int primary key auto_increment, 
	`username` text, 
	`login_date` text);
	

