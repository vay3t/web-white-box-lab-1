<?php

   include ('session.php');
   
?>

    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8" />

        <title>Welcome</title>
        <link rel="stylesheet" href="../assets/css/styles.css" />
        <link rel="stylesheet" type="text/css" href="../css/style.css" media="screen" />

    </head>

    <body>

        <section id="infoPage">
            <h2 id="sign_out"><a href="logout.php">Sign Out</a></h2>
            <img src="../image/logo.jpg" width="164" height="164" />

            <header>
                <h1><?php echo 'Welcome '.$login_session;?></h1>
            </header>
            <div id="upload">
                <h4 class="req-input message-box">Upload your text files here</h4>
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <input type="file" name="file">
                        <br><br>
                        <input type="submit" name="upload" value="Upload">
                    </div>
                </form><?php include ('upload.php');?>
            </div>
		<br>
		<hr style="color:#000000;background-color: #000000;">
		<br>
            <div style="padding:10px;">
                <form method='POST' action=''>
                    <div class="form-group">
                        <label>Search username</label><br>
                        <input style="width:200px" class="form-control" width="50%" placeholder="Enter username" name="name">
                        <br><br>
                        <div align="left" >
                            <button class="btn btn-default" type="submit" name='submit'>Submit Button</button>
			    <?php include ('vendor/twig.php')?>
			<div style="max-height:400px;width:400px;border:0px solid #ccc;font:16px/26px Georgia, Garamond, Serif;overflow:auto;">
			    <?php include ('search.php')?></div>
                        </div>
			<br>
			<div style="color:red"><?php if(isset($numberOfUser)){if($numberOfUser > 0){echo "Number of users: ". $numberOfUser;}} ?></div>
                    </div>
                </form>

            </div>

        </section>

       

        <footer>
            <h2>This lab is created to demonstrate pass-the-hash and blind sql vulnerabilities</h2>
            <a class="tzine" >White box pentesting</a>
        </footer>

    </body>

    </html>
