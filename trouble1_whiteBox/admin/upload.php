
<html>
<body>

<?php

    if(isset($_REQUEST['upload'])){

	if($_FILES["file"]["error"])
	{
		header("Location: welcome.php");
		die();	
	}

	$notAllowed = array('php','php1','php2','php3','php4','php5','php6','php7','phtml','exe','html','cgi','asp','gif','jpeg','png','vb','inf');

	$splitFileName = explode(".", $_FILES["file"]["name"]);

	$fileExtension = end($splitFileName);

	if(in_array($fileExtension, $notAllowed))
	{
		echo "Please upload a TEXT file";
	}
	else{

		echo "Name: ".$_FILES["file"]["name"];
		echo "<br>Size: ".$_FILES["file"]["size"];
		echo "<br>Temp File: ".$_FILES["file"]["tmp_name"];
		echo "<br>Type: ".$_FILES["file"]["type"];

		move_uploaded_file($_FILES["file"]["tmp_name"], "uploads/".$_FILES["file"]["name"]);
	}
    }

?>

</body>
</html>
