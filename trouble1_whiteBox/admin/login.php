<?php
   include("../config.php");
   
   session_start();

   if (isset($_POST['token']))
   
   {
   
    $_SESSION['token'] = $_POST['token'];
   
   }
   
    else{

        if (!isset($_SESSION['token']))
   
        $_SESSION['token'] = sha1(mt_rand() . microtime(TRUE));

    }

   if($_SERVER["REQUEST_METHOD"] == "POST") {
   
   
      // username and password sent from form 
   
      if(isset($_POST['username'])){
   
          $myusername = mysqli_real_escape_string($db,$_POST['username']);
   
          $mypassword = mysqli_real_escape_string($db,$_POST['form_password_hidden']); 
   
          $passtoken = $_SESSION['token'];
     
          $sql = "SELECT id FROM admin WHERE username = '$myusername' and SHA1(CONCAT(password, '$passtoken'))='$mypassword'";
   
          $result = mysqli_query($db,$sql);
   
          $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
   
          $active = $row['active'];
      
          $count = mysqli_num_rows($result);
      
          // If result matched $myusername and $mypassword, table row must be 1 row
  
          if($count == 1) {  
   		
             $_SESSION['login_user'] = $myusername;
	     include('user_log_update.php');
             header("location: welcome.php");   
   
          }else {
   
             $error = "Your Login Name or Password is invalid";
   
          } 
   
      }
   
   }
?>
<html>
   <head>
      <title>Login Page</title>
      <link rel="stylesheet" type="text/css" href="../css/style.css" media="screen" />
      <script language="JavaScript" src="../js/sha-1factory.js" type="text/javascript"></script>
      <script type='application/javascript'>

         function encrypt_password() {
         
         document.getElementById('form_password_hidden').value = hex_sha1(hex_sha1(document.getElementById('password').value) + '<?php echo $_SESSION['token']; ?>');
         
         document.getElementById('password').value = '';
         
         return true;
         
         	}
                
      </script> 
   </head>
   <body bgcolor = "#FFFFFF">
      <div align = "center" >
 	<div style = "width:300px; padding:10px; font-size:25px " align = "center">ADMIN LOGIN</div>
         <div style = "width:300px;background-color:#a2a2a2; border: solid 1px #333333; " align = "left">
            <div style = "background-color:#600000; color:#FFFFFF; padding:3px;"><b>Login</b></div>
            <div style = "margin:30px" >
               <form action = "" method = "post"  >
                  <input type="hidden" name="form_password_hidden" id="form_password_hidden" />
                  <label>UserName  :</label><input type = "text" name = "username" class = "box"/><br /><br />
                  <label>Password  :</label><input type = "password" name = "password" id="password" class = "box" /><br/><br />
                  <input  type = "submit" name="submit" value = "Submit" onclick="return encrypt_password();"/><br/>
                  <a href="../index.php" style="color:#000000";>User login</a></p>
               </form>
               <div style = "font-size:11px; color:#cc0000; margin-top:10px"><?php if(isset($error)){echo $error;} ?></div>
            </div>
         </div>
      </div>
      <br>

      </div>
   </body>
</html>
