<?php
include('session.php');
require 'includes/config.php';
require 'includes/aboutPage.class.php';

$profile = new AboutPage($info);


?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        

        <title>Welcome</title>
        <link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />
        
    </head>
    
    <body>
<div>

         
</div>
		<section id="infoPage">
		<h2 id="sign_out"><a href="logout.php">Sign Out</a></h2>
		<h2 id="sign_out"><a href="user_log.php">Show logs</a></h2>
    		<img src="<?php echo $profile->photoURL()?>"  width="164" height="164" />

            <header>
                <h1><?php echo 'Welcome '.$login_session;?></h1>
                <h2><?php echo $profile->tags()?></h2>
            </header>
            
            <p class="description"><?php echo nl2br($profile->description())?></p>
            
            <a href="<?php echo $profile->facebook()?>" class="grayButton facebook">Find me on Facebook</a>
            <a href="<?php echo $profile->twitter()?>" class="grayButton twitter">Follow me on Twitter</a>
            
            
            
		</section>
        
        <section id="links">
        	<a href="?vcard" class="vcard">Download as V-Card</a>
            <a href="?json" class="json">Get as a JSON feed</a>
            
        </section>
        
        <footer>
	        <h2>This lab is created to demonstrate pass-the-hash and blind sql vulnerabilities</h2>
            <a class="tzine" >White box pentesting</a>
        </footer>
          
    </body>
</html>
