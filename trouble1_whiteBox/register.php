<?php
   include("config.php");
   session_start();

   if($_SERVER["REQUEST_METHOD"] == "POST") {
      // username and password sent from form 
      if(isset($_POST['register'])){
          $myusername = mysqli_real_escape_string($db,$_POST['username']);
          $mypassword = mysqli_real_escape_string($db,$_POST['form_password_hidden']); 
          
          if($myusername == null){
              $error = "username required";
          }else{
              
              $sql = "select * from users where username = '$myusername'";
              $result = mysqli_query($db,$sql);
              $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
              $count = mysqli_num_rows($result);

              if($count > 0){
                  $error = "Username already taken";
              }else{
                
              $sql = "INSERT INTO users (username, password) VALUES ('$myusername','$mypassword')";
              $result = mysqli_query($db,$sql);
                  $msg = "Registered";

              }
         } 
      }
  }
?>
<html>
   
   <head>
      <title>Login Page</title>
      <link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />
      
        <script language="javascript" src="./js/jquery.js" ></script>
        <script language="javascript" src="./js/jquery.sha256.js" ></script>
        <script language="javascript" src="./js/jquery.validate.js" ></script>
	<script language="JavaScript" src="./js/sha-1factory.js" type="text/javascript"></script>
        <script type="application/javascript">
	function encrypt_password() {
	document.getElementById('form_password_hidden').value = hex_sha1(document.getElementById('password').value);
	document.getElementById('password').value = "";
	return true;
}

        </script>  
            
   </head>
   
   <body bgcolor = "#FFFFFF">
	
      <div align = "center">
         <div style = "width:300px; border: solid 0px #333333; " align = "left">
	<div style = "width:300px; padding:10px; font-size:25px " align = "center">REGISTRATION</div>
	<div style = "width:300px;background-color:#a2a2a2; border: solid 1px #333333; " align = "left">
            <div style = "background-color:#600000; color:#FFFFFF; padding:3px;"><b>Register</b></div>
				
            <div style = "margin:30px">
               
               <form action = "" method = "post" name="form_pass_enc" id="form_pass_enc">
                 <input type="hidden" name="form_password_hidden" id="form_password_hidden" />
                  <label>UserName  :</label><input type = "text" name = "username" class = "box"/><br /><br />
                  <label>Password  :</label><input type = "password" name = "password" id="password" class = "box"  /><br/><br />
                  <input id="form_submit" type="submit" value="Submit" name = "register" onclick="return encrypt_password();"/><br/>
                   <a href="login.php" style="color:#000000">login</a>
               </form>
               
               <div style = "font-size:11px; color:#cc0000; margin-top:10px"><?php if(isset($error)){echo $error;} ?></div>
               <div style = "font-size:11px; color:#1ec2ff; margin-top:10px"><?php if(isset($msg)){echo $msg;} ?></div>
					
            </div>
				
         </div>
			
      </div>
      
      <br>

   </body>
</html>
